# Cognitive Workload Database structure

The database contains the subjective folder where all the reported measures from the NASA TLX questionnaire are organized.

The Cognitive workload database is organized so that each subject has two folders (SX, where X = subject ID).
Each subject folder contains the following folders:


### Physiological Data folder (PhysiologicalData_X): 
- LX.acq: the low cognitive workload data for the subject SX
- HX.acq: the high cognitive workload data for the subject SX


### Participant information folder (ParticipantInfo_X):
- X_ParticipantInfo




## Participants 

- 26 subjects participated in the study
- Mean Age: 22.68
- 12 male
- 14 female

### Data Collection

- Data collected using Biopac (ACQ format)

### Collected data have 2 channels:

- Respiration
- Electrocardiogram


## Study Protocol

To induce the two levels of cognitive workload, the validated NASA MATB II  application was utilized. The study was conducted in the Human Interaction lab at Montana State University. Prior to exposing the participants to the cognitive workload tasks, they were outfitted with sensors that captured the physiological signals. For collecting the ECG data, we used a 3-lead chest configuration, and for the respiration data, a respiration belt that went around the chest was utilized. After placing the sensors, the real-time signals were checked, and initial quantitative measures were conducted during a five-minute period in which the participant remained silent and seated in a comfortable posture at the testing station. No participatory tasks were administered during this time.  Following the baseline, the participants performed a practice trial to familiarize themselves with the nature of the task and eliminate the potential for participatory error associated with a lack of confidence with the interface or the need to learn fundamental tasks extemporaneously. Once the participants were comfortable with the MATB II application, they were asked to rest for three minutes to control for the effect of training on physiological data.



As mentioned previously, the participants were exposed to two different cognitive workload tasks. The order of the tasks was counterbalanced to control for order effects. Upon completing each task, the NASA-TLX questionnaire appeared in a full window and the participants used the sliders to select the rating values for the six subscales of the questionnaire. Further, between each task, the participants were given a three-minute rest period to control the carry-forward effect of one experience on the physiological data. During the experience and the rest period, one of the researchers continuously monitored the physiological data to assure that they were collected and transmitted without any issues. The low cognitive workload task lasted for 15 minutes and consisted of a tracking (primary) task. During this task, the participants had to use a joystick to keep a moving target in the center of an inner box presented on the desktop screen. However for the high cognitive workload task, the participants were asked to perform two tasks simultaneously. The primary task is the task of interest (tracking task), and a secondary task is performed alongside the primary task. We utilized a communication task as the secondary task during which participants listened for messages every 30 seconds (like air traffic control requests) and were required to adjust the frequency of a specific radio.





```python

```
